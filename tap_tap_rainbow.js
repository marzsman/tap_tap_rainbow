const black = [0,0,0];
const white = [255,255,255];
const red = [255,0,0];
const orange = [255,102,0];
const yellow = [255,212,0];
const green = [0,255,0];
const blue = [0,0,255];
const purple = [255,0,255];

const rainbow = [red, orange, yellow, green, blue, purple];
let rainbowChallenge = [];
const correct = JSON.stringify([0,1,2,3,4,5]);

const p1Index = 0;
const p2Index = 12*3;
const rainbowIndex = 6*3;

const p1Btns = [1,2,0,4,5,3];
const p2Btns = [11,9,10,8,6,7];

let p1Input = [];
let p2Input = [];

let done = false;

const leds = Uint8ClampedArray(18 * 3);

let toggle = false;

function setColours(colours) {
  //console.log(colours);
  require("neopixel").write(B15, colours);
}

function showError(player) {
  const errors = new Array(6).fill(red);
  leds.set(flat(errors), player);
}

function resetAll() {
  leds.set(new Array(18*3).fill(0,0,18*3), p1Index);
  setColours(leds);
}

function flat(array) {
  return array.reduce((acc, currentValue) => {
    return acc.concat(currentValue);
  }, [] );
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

function setButton(keyIndex, playerIndex) {
  // console.log('set light at', (keyIndex + playerIndex));
  leds.set(new Array(3).fill(255), playerIndex+(keyIndex*3));
  setColours(leds);
}


function btnHandler() {
  //leds.set(flat(rainbow), p1Index);
  //leds.set(new Array(18).fill(255,0,6*3), p2Index);
  done = false;
  
  const shuffledRainbow = shuffle(rainbow.slice());
  
  
  rainbowChallenge = [];
  shuffledRainbow.forEach((item) => {
   rainbowChallenge.push(rainbow.indexOf(item));
  });
  
  console.log('challenge: ', rainbowChallenge);
  
  leds.set(new Array(18*3).fill(0,0,18*3), p1Index);
  //leds.set(new Array(18).fill(0,0,6*3), p2Index);
  leds.set(flat(shuffledRainbow), rainbowIndex);
  
  p1Input = [];
  p2Input = [];
  
  //leds.set(flat(rainbow), rainbowIndex);
  
  //showError(player1Index);
  setColours(leds);
  
  toggle = !toggle;
}

function onKey(key) {
  if(done) return;
  
  console.log('--------------------------------');
  
  console.log('Button index', p2Btns.indexOf(key));
  
  
  if(p1Btns.includes(key)){
    const btnIndex = p1Btns.indexOf(key);
    const colorIndex = rainbowChallenge[btnIndex];
    if(!p1Input.includes(colorIndex)){
      p1Input.push(colorIndex);
      setButton(p1Btns.indexOf(key), p1Index);
    }
  }
  
  
  if(p2Btns.includes(key)){
    const btnIndex = p2Btns.indexOf(key);
    const colorIndex = rainbowChallenge.slice().reverse()[btnIndex];
    if(!p2Input.includes(colorIndex)){
      p2Input.push(colorIndex);
      setButton(p2Btns.indexOf(key), p2Index);
      console.log('p2Input', p2Input);
    }
  }

  if((p1Input.length === 6 || p2Input.length === 6)) {
    console.log('p1 final', p1Input);
    console.log('p2 final', p2Input);
    if(JSON.stringify(p1Input) === correct) {
      done = true;
      console.log('Player 1 WON!!!!');
    }
    
    console.log(JSON.stringify(p2Input));
    if(JSON.stringify(p2Input) === correct) {
      done = true;
      console.log('Player 2 WON!!!!');
    }
  }
}

setWatch(btnHandler, BTN, { repeat: true, edge:'rising', debounce: 50 });

require("KeyPad").connect([B2,B3,B4],[B6,B7,B8,B9], onKey);

resetAll();
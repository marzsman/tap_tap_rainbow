/*
var i2c=I2C1;
i2c.setup({scl:B6,sda:B7});
var address = 0; //this is the address, set by the address pins.
var RST=A8; //pin connected to reset. Omit if reset is not connected.

// for MCP23017
var port=require("MCP23017").connect(i2c,RST,address);
var pins = [port.B5, port.B4, port.B3, port.B2, port.B1, port.B0];
//for (var i in pins) {
//    pins[i].mode("input_pullup");
//}


 //console.log(port.B0.read());

port.B0.mode('input');
console.log(port.B0.read());
*/



function isDeviceOnBus(i2c,id) {
      try {
        return i2c.readFrom(id,1);
      }
      catch(err) {
        return -1;
      }
}
function detect(i2c,first, last) {
      first = first | 0;
      last = last | 0x77;
      var idsOnBus = Array();
      for (var id = first; id <= last; id++) {
        if ( isDeviceOnBus(i2c,id) != -1) {
          idsOnBus.push(id);
        }
      }
      return idsOnBus;
}
// Espruino boards
I2C1.setup( {scl: B8, sda: B9} );

console.log('I2C detect as array:',detect(I2C1));